package com.example.karyawan.DTO;

import com.example.karyawan.entity.DetailEmployee;
import com.example.karyawan.entity.Rekening;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EmployeeDTO {
    public Long id;

    public String name;

    public String address;

    // 2016-01-01
    private Date dob;

    public  String status = "active";

    private Long detailEmployeeId;

}

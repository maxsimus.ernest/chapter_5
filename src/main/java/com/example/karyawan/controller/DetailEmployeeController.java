package com.example.karyawan.controller;

import com.example.karyawan.DTO.EmployeeDTO;
import com.example.karyawan.entity.DetailEmployee;
import com.example.karyawan.entity.Employee;
import com.example.karyawan.repository.EmployeeRepository;
import com.example.karyawan.service.DetailEmployeeImpl;
import com.example.karyawan.service.EmployeeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/v1/detail-employee")
public class DetailEmployeeController {
    @Autowired
    public DetailEmployeeImpl detailEmployeeImpl;

    com.example.Aplikasi.Karyawan.SimpleStringUtils simpleStringUtils = new com.example.Aplikasi.Karyawan.SimpleStringUtils();

    @GetMapping(value = {"", "/"})
    public Page<DetailEmployee> findAll(@RequestParam(defaultValue = "0") int pageNumber,
                                  @RequestParam(defaultValue = "100") int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return detailEmployeeImpl.findAll(pageable);
    }

    @PostMapping(value = {"", "/"})
    public Map testSave(@RequestBody DetailEmployee request) {
//        Map map = new HashMap<>();
//        map.put("data", request);
//        return map;
        return detailEmployeeImpl.save(request);
    }
}

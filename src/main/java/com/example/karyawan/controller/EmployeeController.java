package com.example.karyawan.controller;

import com.example.Aplikasi.Karyawan.SimpleStringUtils;
import com.example.karyawan.DTO.EmployeeDTO;
import com.example.karyawan.entity.Employee;
import com.example.karyawan.repository.EmployeeRepository;
import com.example.karyawan.service.EmployeeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {
    @Autowired
    public EmployeeImpl employeeImpl;

    @Autowired
    public EmployeeRepository employeeRepository;

    SimpleStringUtils simpleStringUtils = new SimpleStringUtils();

    @GetMapping(value = {"", "/"})
    public Page<Employee> findAll(@RequestParam(defaultValue = "0") int pageNumber,
                                  @RequestParam(defaultValue = "100") int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return employeeImpl.findAll(pageable);
    }

    @PostMapping(value = {"", "/"})
    public Map testSave(@RequestBody EmployeeDTO request) {
//        Map map = new HashMap<>();
//        map.put("data", request);
//        return map;
        return employeeImpl.save(request);
    }

    @PutMapping({"{id}", "{id}/"})
    public ResponseEntity<Map> update(@PathVariable Long id, @RequestBody Employee employee) {
        return new ResponseEntity<Map>(employeeImpl.update(id, employee), HttpStatus.OK);
    }

    @DeleteMapping({"{id}", "{id}/"})
    public Map delete(@PathVariable Long id) {
        return employeeImpl.delete(id);
    }

    @GetMapping(value = {"/{id}", "/{id}/"})
    public ResponseEntity<Map> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<Map>(employeeImpl.delete(id), HttpStatus.OK);
    }


}



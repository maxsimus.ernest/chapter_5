package com.example.karyawan.logger;

import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

@Slf4j
public class TestLogger {
    @Test
    public void testLogger(){
        log.trace("this is trace log");
        log.debug("this is debug log");
        log.info("this is info log");
        log.warn("this is warn log");
        log.error("this is error log");
    }
}

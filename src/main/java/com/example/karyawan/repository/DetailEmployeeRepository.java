package com.example.karyawan.repository;


import com.example.karyawan.entity.DetailEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailEmployeeRepository extends JpaRepository<DetailEmployee, Long>, JpaSpecificationExecutor<DetailEmployee> {

}

package com.example.karyawan.repository;

import com.example.karyawan.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee>  {
    //JPA Query
    @Query(value = "select c from Employee c WHERE c.id = :idEmployee", nativeQuery = false)
    public Employee getById(@Param("idEmployee") Long idEmployee);

    //Native Query : menggunakan JPAQL
    @Query(value = "select  e from employee e where  id = :idEmployee;",nativeQuery = true)
    public Object getByIdNativeQuery(@Param("idEmployee") Long idEmployee);

}

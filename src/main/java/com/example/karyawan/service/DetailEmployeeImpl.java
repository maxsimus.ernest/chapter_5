package com.example.karyawan.service;

import com.example.karyawan.entity.DetailEmployee;
import com.example.karyawan.entity.Employee;
import com.example.karyawan.repository.DetailEmployeeRepository;
import com.example.karyawan.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DetailEmployeeImpl implements DetailEmployeeService{

    @Autowired
    public DetailEmployeeRepository detailEmployeeRepository;

    @Override
    public Page<DetailEmployee> findAll(Pageable pageable) {
        return detailEmployeeRepository.findAll(pageable);
    }

    @Override
    public Map save(DetailEmployee detailEmployee) {
        return Response.success(detailEmployeeRepository.save(detailEmployee));
    }
}

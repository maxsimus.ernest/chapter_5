package com.example.karyawan.service;

import com.example.karyawan.DTO.EmployeeDTO;
import com.example.karyawan.entity.DetailEmployee;
import com.example.karyawan.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface DetailEmployeeService {

    Page<DetailEmployee> findAll(Pageable pageable);

    Map save(DetailEmployee detailEmployee);
}

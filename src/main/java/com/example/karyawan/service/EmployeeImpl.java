package com.example.karyawan.service;

import com.example.karyawan.DTO.EmployeeDTO;
import com.example.karyawan.entity.DetailEmployee;
import com.example.karyawan.entity.Employee;
import com.example.karyawan.repository.DetailEmployeeRepository;
import com.example.karyawan.repository.EmployeeRepository;
import com.example.karyawan.utils.Response;
import com.example.karyawan.utils.generalUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class EmployeeImpl implements EmployeeService {

    // step 1 : koneksi ke database ?
    @Autowired
    public EmployeeRepository employeeRepository;

    @Autowired
    public DetailEmployeeRepository detailEmployeeRepository;

    @Override
    public Page<Employee> findAll(Pageable pageable) {
        return employeeRepository.findAll(pageable);
    }


    @Override
    public Map save(EmployeeDTO employee) {

        Employee newEmployee = new Employee();
        newEmployee.setName(employee.getName());
        newEmployee.setAddress(employee.getAddress());
        newEmployee.setDob(employee.getDob());
        newEmployee.setStatus(employee.getStatus());
        if (generalUtils.isNull(employee.getDetailEmployeeId())){
            return Response.notFound("DetailEmployeeId");
        }else{
            DetailEmployee detailEmployee = detailEmployeeRepository.getById(employee.getDetailEmployeeId());
            if(generalUtils.isNull(detailEmployee)){
                return Response.notFound(detailEmployee);
            }
            newEmployee.setDetailEmployee(detailEmployee);
        }
        Employee doSave = employeeRepository.save(newEmployee);

        return Response.success(doSave);
    }

    @Override
    public Map update(Long id, Employee employee) {
        /*
        1. ngecek ke db base param ID
        2. jika ditemuakan baru di update
        3. jika tidak ditemukan: di tolak -return data notfound.
         */

        Employee chekData = employeeRepository.getById(id);
        if(chekData == null){
            return Response.notFound();
        }
        chekData.setAddress(employee.getAddress());
        chekData.setName(employee.getName());

        Employee doUpdate = employeeRepository.save(chekData);

        return Response.success(doUpdate);
    }

    @Override
    public Map delete(Long id) {

        Employee chekData = employeeRepository.getById(id);
        if(chekData == null){
            return Response.notFound();
        }
        chekData.setDeleted_date(new Date());
        Employee doDelete = employeeRepository.save(chekData);

//        // deleted permanent ?
//        employeeRepository.delete(chekData);

        return Response.success(doDelete);
    }

    @Override
    public Map getByID(Long id) {
        Map map = new HashMap();
        Optional<Employee> getBaseOptional = employeeRepository.findById(id);
        if(getBaseOptional.isEmpty()){
            return Response.notFound();
        }
        map.put("data",getBaseOptional.get());
        return map;
    }
}


package com.example.karyawan.service;

import com.example.karyawan.DTO.EmployeeDTO;
import com.example.karyawan.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface EmployeeService {

    Page<Employee> findAll(Pageable pageable);

    Map save(EmployeeDTO employee);

    Map update(Long id, Employee employee);

    Map delete(Long employee);

    Map getByID(Long employee);
}

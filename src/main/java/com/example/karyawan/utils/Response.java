package com.example.karyawan.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Response {
    public static Map success(Object object){
        Map<String, Object> map = new HashMap<>();

        map.put("response", 200);
        map.put("data", object);
        map.put("message", "success");

        return map;
    }

    public static Map notFound(){
        Map<String, Object> map = new HashMap<>();

        map.put("response", 400);
        map.put("message","data not found");

        return map;
    }

    public static Map notFound(Object object){
        Map<String, Object> map = new HashMap<>();

        map.put("response", 400);
        map.put("message",(object + " not found"));

        return map;
    }

}
